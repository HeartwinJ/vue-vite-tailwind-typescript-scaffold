const defaultTheme = require("tailwindcss/defaultTheme");
const formKitTailwind = require("@formkit/themes/tailwindcss");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "ram-red": "#d41212",
        "ram-grey": "#666366",
        "ram-teal": "#279989",
        "ram-yellow": "#f7b512",
        "ram-black": "#000000",
        "ram-lime": "#97d700",
      },
      fontFamily: {
        inter: ['"Inter var"', ...defaultTheme.fontFamily.sans],
        rambla: ["Rambla", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [
    require("@tailwindcss/forms"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/aspect-ratio"),
    formKitTailwind,
  ],
};
