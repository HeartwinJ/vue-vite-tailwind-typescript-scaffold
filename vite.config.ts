/// <reference types="vitest" />
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";
import {
  BugsnagBuildReporterPlugin,
  BugsnagSourceMapUploaderPlugin,
} from "vite-plugin-bugsnag";

export default ({ mode }) => {
  const prefixes = ["VITE_", "RAM_", "BGSNG_", "AUTH0_", "PH_"];
  const envVars = loadEnv(mode, process.cwd(), prefixes);
  const isProd = mode === "production";
  return defineConfig({
    envPrefix: prefixes,
    test: {
      environment: "happy-dom",
    },
    plugins: [
      vue(),
      isProd &&
        BugsnagBuildReporterPlugin({
          appVersion: envVars.RAM_APP_VERSION,
          apiKey: envVars.BGSNG_API_KEY,
          releaseStage: mode,
        }),
      isProd &&
        BugsnagSourceMapUploaderPlugin({
          appVersion: envVars.RAM_APP_VERSION,
          apiKey: envVars.BGSNG_API_KEY,
          overwrite: true,
        }),
    ],
    resolve: {
      alias: {
        "@": resolve(__dirname, "src"),
      },
    },
    server: {
      open: true,
    },
    build: {
      chunkSizeWarningLimit: 5000,
      sourcemap: true,
    },
  });
};
